/**
 * @author Marc Dean Jr.
 * CIS 410 - Computer Networks
 * Program004 - Chat Client
 */

package client;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import tools.KeyboardToQueue;
import io.Reader;
import io.Writer;


/**
 * 
 * The ChatClient is the main program for the chat client project.
 * It is started with CLA's. 
 * 
 *    --local <port> is the local port where this peer expects communication.
 *
 *    --remote <port> the port number where the peer with which this program
 *      is communicating expects communication.
 *
 *    --machine <name> the name of the machine on which the other peer is running
 *
 *	When started, the client will spin up threads to handle input/output to/from
 *	multiple other peers. The ChatClient will also manage the threads if there is 
 *  some sort of error or disconnection.
 *
 */

public class ChatClient {
	
	/** Communication Socket */
	private static DatagramSocket socket;
	
	/** Local Port expecting communication */
	private static int localPort;
	
	/** Remote port expecting communication */
	private static int remotePort;
	
	/** Machine name where other peer is running */
	private static String remoteMachineName;
	
	/** Command-Line Switches */
	private static String ARG_LOCAL_PORT = "--local";
	private static String ARG_REMOTE_PORT = "--remote";
	private static String ARG_MACHINE_NAME = "--machine";
	
	/** Number of command line arguments */
	private final static int NUM_ARGS = 6;
	
	/** Contact List */
	private static ArrayList<Address> addressBook;
	
	/** Message Queue */
	private static BlockingQueue<String> messageQueue;
	
	/**
	 * 
	 * @param args - Command line arguments to process as described
	 * above.
	 * 
	 * Handles CLA's, Error Messages, Starts Read/Write Threads,
	 * manages contact list
	 * 
	 * @throws SocketException 
	 */
	
	public static void main(String[] args) throws SocketException {
		// Error handling, CLA handling
		argSetup(args);
		addressBook = new ArrayList<Address>();		
		try {
			Address add = new Address(InetAddress.getByName(remoteMachineName),
					remotePort);
			addressBook.add(add);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		run();
		
		
	}
	
	/** 
	 *  Begin reading and writing threads to pass messages
	 */
	public static void run() {
		
		// Linked List implementation of a queue for
		// messages
		
		messageQueue = new LinkedBlockingQueue<String>();
		
		
		try {
			
			
			// Reads keyboard information and places it into queue
			KeyboardToQueue keyboard = new KeyboardToQueue(System.in, 
					messageQueue);
			keyboard.start();
			
			// Start reader thread
			socket = new DatagramSocket(localPort);
			Reader read = new Reader(socket, addressBook);
			
			
			// start write thread
			Writer write = new Writer(socket, messageQueue, read);
			read.start();
			write.start();

			while(true) {}
		} catch(SocketException e) {
			System.err.println("Couldn't establish local or distant connection");
			e.printStackTrace();
			System.exit(2);
		}
		
		
	}
	
	/**
	 * Prints usage and syntax for running the program
	 */
	private static void usage() {
		System.out.println("--local <port> is the local port where this peer expects communication."
				+ "\n--remote <port> the port number where the peer with which this program "
				+ "is communicating expects communication.\n"
				+"--machine <name> the name of the machine on which the other peer is running");
		
		System.exit(0);
 
	}
	
	
	/**
	 * 
	 * @param args - CLA of this session
	 * argSetup handles the command line switches
	 * and the storage of these arguments and handles
	 * invalid switches.
	 * 
	 */
	private static void argSetup(String[] args) {
		
		if(args.length != NUM_ARGS)
			usage();
		
		int argNdx = 0;
		
		while(argNdx < args.length) {
			if(args[argNdx].equals(ARG_LOCAL_PORT)) {
				argNdx++;
				localPort = Integer.parseInt(args[argNdx]);
			} else if(args[argNdx].equals(ARG_MACHINE_NAME)) {
				argNdx++;
				remoteMachineName = args[argNdx];
			} else if(args[argNdx].equals(ARG_REMOTE_PORT)) {
				argNdx++;
				remotePort = Integer.parseInt(args[argNdx]);
			} else {
				System.out.println("Invalid switch " + args[argNdx]);
				usage(); // this ends the program
			}
			argNdx++;
		}
		
		
	}
	
	
	
	

}
