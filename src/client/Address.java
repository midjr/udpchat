/**
 * 
 * @author Marc Dean Jr.
 * CIS 410 - Computer Networks
 * Program004 - Chat Client
 * 
 */

package client;

import java.net.InetAddress;


/**
 * 
 * The address stores necessary information to send packets
 * to other peers for the UDP Chat program. We will be able to 
 * store Addresses into a "book" and then when people connect, 
 * save their address in order to send messages to everyone
 * during the next message that is sent.
 *
 */

public class Address {
	
	/** InetAddress of distant machine */
	private InetAddress distIAddress;
	
	/** Port number of distant machine */
	private int distPortNumber;
	
	/** If connected or not still, should remove from list? */
	private boolean disconnected;
	
	/** 
	 * 
	 * @param add - InetAddress of distant machine
	 * @param port - Port number allowing communication at distant machine
	 * Instantiate an Address instance with an InetAddress and port number
	 * 
	 */
	public Address(InetAddress add, int port) {
		this.distIAddress = add;
		this.distPortNumber = port;
		this.disconnected = false;
	}
	
		
	/**
	 * 
	 * @return if is disconnected or experienced
	 * an exception
	 */
	public boolean isDisconnected() {
		return this.disconnected;
	}
	
	/**
	 * Sets disconnect flag to true so it will be cleaned out
	 * 
	 */
	public void setDisconnected() {
		this.disconnected = true;
	}
	
	/**
	 * 
	 * @return InetAddress of the distant machine
	 */
	public InetAddress getInetAddress() {
		return this.distIAddress;
	}
	
	/**
	 * 
	 * @return port number of distant machine accepting
	 * communication
	 * 
	 */
	
	public int getPortNumber() {
		return this.distPortNumber;
	}
	
	/**
	 *  @return String representation of the Address
	 */
	public String toString() {
		return this.distIAddress.toString() + ':' + this.distPortNumber ;
	}
	
}
