/**
  * Marc Dean Jr.
  * CIS 410 - Computer Networks
  * Program 004 - Chat Client
  */


package tools;

import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;



/**
 * Thread that copies lines typed by the user into the message queues.
 * 
 * Based off @author blad KeyboardToQueue.java for UDP communication
 *
 * @author Marc Dean
 */
public class KeyboardToQueue extends Thread {
  private final Scanner fin;
  private final BlockingQueue<String> messageQueue;

  /**
   * Construct a new KeyboardToQueue instance. Set the scanner to scan the
   * keyboard and initialize the server and regular message queue values (these
   * are where server and regular messages are copied after a line is read).
   *
   * @param in
   *          Where to read information from (permits redirection to a file
   *          rather than hard coding the keyboard)
   * @param queue
   *          The regular message queue for messages to be dispatched to all
   *          members of the chat group
   * @param serverQueue
   *          The message queue for messages to be sent to the server.
   */
  public KeyboardToQueue(InputStream in,
    BlockingQueue<String> messageQueue) {
    this.messageQueue = messageQueue;
    this.fin = new Scanner(in);
  }

  @Override public void run() {
    String line;
    System.out.print("> ");

    while ((line = fin.nextLine().trim()) != null) {

      try {

          messageQueue.put(new String(line));

      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }

      System.out.print("> ");
    }
  }
}
