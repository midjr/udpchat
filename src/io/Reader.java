/**
 * 
 * @author Marc I Dean Jr.
 * CIS 410 - Computer Networks
 * Program004 - Chat Client
 * 
 */


package io;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import client.Address;

/**
 * 
 * Reader handles reading messages from various connections
 * using a single thread. Part of the functionality of reader
 * is to give the writer addresses that it receives from so that it can
 * broadcast to multiple people.
 *
 */

public class Reader extends Thread {
	
	/** Data Received */
	byte[] receiveData;
	
	/** Communication socket */
	DatagramSocket socket;
	
	/** maximum buffer size for incoming transmissions */
	private final int BUFF_SIZE = 256;
	
	/** List of addresses */
	private static ArrayList<Address> addressBook;
	
	/** Create the socket, set buffer size */
	public Reader(DatagramSocket socket, ArrayList<Address> add) {
		this.socket = socket;
		this.addressBook = add;
		receiveData = new byte[BUFF_SIZE];
	}
	
	public void run() {
		try {
		
		    while(true) {
			    DatagramPacket packet = new DatagramPacket(this.receiveData, 
					    receiveData.length);
			    socket.receive(packet);
			    Address addRecord = new Address(packet.getAddress(), packet.getPort());
			
			    if(!in(addRecord)) 
			        addressBook.add(addRecord);
			   
			
			    System.out.println(addRecord + " - " + new String(packet.getData(),
					    0, packet.getLength()));
			    
			   
			}
		} catch (SocketException e) {
			// output thread closed socket
		} catch (IOException ioe) {
			System.err.println(ioe);
		} 
	}
	


    /** 
      * @return size of the address book
      */
    public int addressSize() {
        return this.addressBook.size();
    }
    
    /**
      * @return address at certain index
      */
      public Address getIndex(int index) {
        return this.addressBook.get(index);
      }
      
      /**
	  * Checks if the contact is in the list and if not
	  * adds it.
	  *
	  */
	public boolean in(Address add) {
    
        String addy = add.toString();
	// trim machine name off of address if added on
	addy = addy.substring(addy.indexOf('/'), addy.length());
        for(int i = 0; i < this.addressBook.size(); i++) {
	    String check = addressBook.get(i).toString();
	    check = check.substring(check.indexOf('/'),check.length());
            if(addy.equals(check))
                return true;
        }
    
        return false;
	}
	
}
