/**
 * 
 * Marc I Dean Jr.
 * CIS 410 - Computer Networks
 * Program004 - Chat Client
 * 
 */

package io;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.ArrayList;


/**
 * 
 * The writer watches the keyboard and sends the messages
 * to a specific Address object (InetAddress : port pair)
 * This is a threaded application. It handles new contacts
 *
 */
public class Writer extends Thread {
	
	/** Message Queue */
	private BlockingQueue<String> messageQueue;
	
	/** data to send in form of byte array */
	private byte[] data;
	
	/** Communication socket */
	private DatagramSocket socket;
	
	/** Reader reference for addresses */
	private Reader reader;
	
	
	/**
	 * 
	 * @param socket - DatagramSocket for communication
	 * @param msg - BlockingQueue of strings of the messages to send
	 * @param r - reader object for list of contacts
	 * 
	 */
	public Writer(DatagramSocket socket, 
			BlockingQueue<String> msg, Reader r) {
		this.socket = socket;
		this.messageQueue = msg;
		this.reader = r;
	}
	
	/**
	 * Wait for message to send and send it using a socket and 
	 * the address of the recipient. 
	 */
	public void run() {
		
		try {
		  while(true) {
			String message = this.messageQueue.take() + '\n';
			this.data = message.getBytes();
			
						
				try {
				for(int i = 0; i < this.reader.addressSize(); i++) {
				   DatagramPacket pkt = new DatagramPacket(this.data, 
							this.data.length, 
							this.reader.getIndex(i).getInetAddress(), 
							this.reader.getIndex(i).getPortNumber());
				
					socket.send(pkt);
			    }
				} catch (SocketException e) {
					e.printStackTrace();
				}
				
			}
		  	
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e ) {
			e.printStackTrace();
			
		}
		
		
		
	}
	
	
	
	
}
